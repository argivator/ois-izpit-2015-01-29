var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/api/prijava', function(req, res) {
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var success = (preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo));
	var napaka = "";
	if (uporabniskoIme.length == 0 || geslo.length == 0) napaka = "Napačna zahteva";
	if (!success) napaka = "Avtentikacija ni uspela";
	
	res.send({status: success, napaka: napaka});
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var naslov;
	var sporocilo;
	if(preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo)){
		naslov = "Uspešno"
		sporocilo = "uspešno prijavljen v sistem";
	} else {
		naslov = "Napaka";
		sporocilo = "nima pravice prijave v sistem";
	}
	res.send("<html><title>" + naslov + "</title><body><p>Uporabnik <b>" + uporabniskoIme + "</b> " + sporocilo + "!</p></body></html>");
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];

var podatkiDatotekaStreznik = JSON.parse(fs.readFileSync(__dirname + "/public/podatki/uporabniki_streznik.json").toString());


function preveriSpomin(uporabniskoIme, geslo) {
	for (i in podatkiSpomin) {
		username = podatkiSpomin[i].split("/")[0];
		password = podatkiSpomin[i].split("/")[1];
		if (username == uporabniskoIme && password == geslo) return true;
	}
	return false;
}


function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	for (i in podatkiDatotekaStreznik) {
		username = podatkiDatotekaStreznik[i]["uporabnik"];
		password = podatkiDatotekaStreznik[i]["geslo"];
		if (username == uporabniskoIme && password == geslo) return true;
	}
	return false;
}